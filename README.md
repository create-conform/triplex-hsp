# triplex-hsp

triplex module that enables you to serve handlebars server pages.

## Installation

```sh
npm install triplex-hsp
```

## Usage

If you want to use this module, you need to add this module to the triplex configuration.

**Hint** You can pass options to triplex in multiple ways. Check the triplex documentation for more information.

```json
{
    "modules" : {
        "triplex-hsp" : {}
    }
}
```

The module will serve and render handlebars pages from a given path. These pages can have an accompanying javascript file that is executed on the server. For example, this enables you to perform database operations on the server side, and pass the results in a data object to the handlebars renderer for serving to the client.

When the client does not request a specific document (ex. "http://localhost/"), the server will render "index.hbs" if it exists.

### Options

| Name | Description | Default |
| ---- | ----------- | ------- |
| endpoint | The name of the endpoint to use for serving the handlebars pages. | "default" |
| route | The prefix of the route, this prefix is the root of the handlebars pages path. For example, if you specify "/intranet" the index page will be served at "/intranet/index.hbs" | |
| path | The filesystem path to the root of the handlebars pages. The default value is the current working directory where triplex is launched in. | "./" |
| sandbox | An object containing functions to be exposed to the server side javascript accompanying the hanblebars file. For example, this could be usefull for passing api functions when you create your own triplex module using triplex-hsp. | |
| acl | The name of the acl to use for the route. See triplex-acl for more information. | |

### Example

In the example below, an hsp server is created for the default endpoint's root, and serves files from "C:\pub\www". The pages will be served at "http://localhost/".

Then, the dispatcher module is set to use this database.

```json
{
    "modules" : {
        "triplex-endpoint" : {},
        "triplex-hsp" : {
            "path" : "C:\\pub\\www"
        }
    }
}
```

## Building A Custom Module Using triplex-hsp

It could be usefull to create your own module, that has an api, some handelbars server pages and perhaps more. Create a custom module as usual (see the triplex documentation), and instantiate triplex-hsp from within the start function. This way you can pass your custom api functions in the sandbox option.

In this example, we have a directory with the following contents:

```
custom-module/
├── public/
│   ├── index.hbs
│   └── index.hbs.js
├── custom-module.js
└── package.json
```

It basically comes down to a triplex module file called custom-module.js and the public folder will be used to store files that triplex-hsp needs to serve.

### custom-mdoule.js

This file contains the triplex module. It exports a class that needs to be instantiated, that contains a start and stop function.

```javascript
/////////////////////////////////////////////////////////////////////////////////////////////
//
// custom-module.js
//
//    Triplex module.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
/////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var path = require("path");

/////////////////////////////////////////////////////////////////////////////////////////////
//
// CustomModule Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function CustomModule() {
    var self = this;
    var hsp;

    ////////////////////////////////////////////////////////////////////
    //
    // Api
    //
    ////////////////////////////////////////////////////////////////////
    this.api = {
        "foo" : function() {
            return "bar";
        }
    };

    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        return new Promise(function(resolve, reject) {
            // create hsp instance, that will serve files from
            // a directory called "public" in the module directory.
            hsp = require("triplex-hsp")({ 
                "route" : "/custom-module",
                "path" : path.join(__dirname, "public"),
                "sandbox" : {
                    "api" : self.api
                }
            }, shared);
            // start our newly created hsp instance
            hsp.start();

            // done, resolve start promise
            resolve();
        });
    };

    this.stop = function() {
        return new Promise(function(resolve, reject) {
            // stop the hsp instance, and resolve the stop promise
            return hsp.stop().then(resolve).catch(reject);
        });
    };
}


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = CustomModule;
```

### public/index.hbs

A handlebars html file that will be available at http://localhost/.

```html
<html>
    <body>
        {{testValue}}
    </body>
</html>
```

### public/index.hbs.js

the server side javascript that will be executed when the user requests index.hbs at http://localhost/.

```javascript
// create a data object
var data = {};

// call the api function "foo", and store the result
data.testValue = api.foo();

// render the reponse, and pass the data object for handlebars
response.render(data);
```
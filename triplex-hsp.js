/////////////////////////////////////////////////////////////////////////////////////////////
//
// Triplex HSP
//
//    Triplex module that enables you to serve handlebars server pages.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
///////////////////////////////////////////////////////////////////////////////////////////// 
//
// Privates
//
///////////////////////////////////////////////////////////////////////////////////////////// 
var express =      require("express");
var expressHbs =   require("express-handlebars");
var handlebars =   require("handlebars");
    Error =        require("error");
var path =         require("path");
var fs =           require("fs");
var vm =           require("vm");
var m =            require("module");


/////////////////////////////////////////////////////////////////////////////////////////////
//
// TriplexEndpoint Class
//
/////////////////////////////////////////////////////////////////////////////////////////////
function TriplexHSP(options, shared) {
    if (!(this instanceof TriplexHSP)) return new TriplexHSP(options, shared);

    var self = this;
    var router;
    var endpoint;
    var route;

    shared =           shared || {};
    shared.endpoints = shared.endpoints || {};


    this.options =     options || {};


    ////////////////////////////////////////////////////////////////////
    //
    // handlebars Templating Middleware
    //
    ////////////////////////////////////////////////////////////////////
    function setEndpointEngine() {
        shared.endpoints[endpoint].engine("hbs", expressHbs({
            "extname": ".hbs", 
            "helpers": {
            "escape": function(variable) {
                return variable != null && 
                    typeof variable.toString === "function" ?
                    variable.toString().replace(/(['"])/g, "\\$1") : "";
                }
            } 
        }));
    }

    if (shared.endpoints[endpoint]) {
        setEndpointEngine();
    }
    else {
        shared.endpoints.on("add", function(edp) { if (edp.name == endpoint) { setEndpointEngine(); } });
    }

    ////////////////////////////////////////////////////////////////////
    //
    // Service Controllers
    //
    ////////////////////////////////////////////////////////////////////
    this.start = function() {
        return new Promise(function(resolve, reject) {
            endpoint = self.options.endpoint || "default";
            route = self.options.route;

            ////////////////////////////////////////////////////////////////////
            //
            // Create Router
            //
            ////////////////////////////////////////////////////////////////////
            router = express.Router();


            ////////////////////////////////////////////////////////////////////
            //
            // Serve Handlebars Pages
            //
            ////////////////////////////////////////////////////////////////////
            function processURI(req, res) {
                console.log("[" + req.connection.remoteAddress + "] " + req.method + " " + req.url);
                
                var originalUrl = req.url;

                // strip url parts
                var idx = req.url.lastIndexOf("#");
                if (idx > -1) {
                    req.url = req.url.substr(0, idx);
                }
                idx = req.url.lastIndexOf("?");
                if (idx > -1) {
                    req.url = req.url.substr(0, idx);
                }

                //
                // if a path is requested instead of a document, serve the default index.hbs in that path
                //
                if (req.url.lastIndexOf("/") == req.url.length - 1) {
                    req.url += "index.hbs";
                }
            
                //
                // do not serve the server side files
                //
                if (//req.url == "/favicon.ico" || 
                    req.url.lastIndexOf(".hbs.js") == req.url.length - 7) {
                        res.sendStatus(404);
                    return;
                }

                var data = {}; 
                var p = self.options.path.replace(/\\/g, "/") || "./";
                    p = p.endsWith("/")? p.substr(0,p.length - 1) : p; 
                var url = req.url;

                if (route) {
                    url = req.url.substr(route.length);
                    url = url.startsWith("/")? url : "/" + url;
                }

                var file = path.resolve(process.cwd(), p + url);

                //
                // if there is no specific file requested, and the path does not end with a slash, redirect to the path
                //
                if (!req.url.endsWith("/") && fs.existsSync(file) && fs.lstatSync(file).isDirectory()) {
                    console.log("REDIRECT " + req.url + "  TO  " + (req.url + "/"));
                    return res.redirect(req.url + "/");
                }
            
                //
                // set the isPostback preperty in the request object
                //
                req.isPostback = true;
                if (Object.keys(req.body).length === 0 && req.body.constructor === Object) {
                    req.isPostback = false;
                }
            
                var originalRender = res.render;
        
                if (!fs.existsSync(file)) {
                    if (!fs.existsSync(file + ".hbs")) {
                        res.sendStatus(404);
                        return;
                    }

                    file += ".hbs";
                }

                if (file.endsWith(".hbs")) {
                    function render(data) {
                        if (data instanceof Error) console.error(data);

                        res.render = originalRender;
                        try {
                            res.render(file, data);
                        }
                        catch(e){
                            console.error(e);
                        }
                    }
            
                    res.render = render;
                    req.url = originalUrl;

                    try {
                        var code            = fs.readFileSync(file + ".js");
                        var mod             = new m.Module(file + ".js", module);
                        var sandbox         = self.options.sandbox || {};
                        sandbox.module      = mod;
                        sandbox.__filename  = file + ".js";
                        sandbox.__dirname   = path.dirname(file);
                        sandbox.shared      = shared;
                        sandbox.global      = global;
                        sandbox.request     = req;
                        sandbox.response    = res; 
                        sandbox.require     = function(path) {
                            return mod.require(path);
                        }
                        sandbox.console     = console;

                        mod.filename = sandbox.__filename;
                        mod.paths =    m.Module._nodeModulePaths(sandbox.__dirname);

                        vm.runInNewContext(code, sandbox, { "timeout" : 10000 });
                    }
                    catch(e) {
                        if (e.code != "ENOENT") {
                            console.error(e);
                        }
                        render(data);
                    }
                } else {
                    res.send(fs.readFileSync(file));
                }
            }
            
            function endpointAdded() {
                var r = route;
                r = !route.endsWith("/")? route + "/" : route;
                r += "*";

                if (self.options.acl) {
                    router.all(r, shared.acl[self.options.acl], processURI);
                }
                else {
                    router.all(r || "*", processURI);
                }

                shared.endpoints[endpoint].use(router);
            }

            if (shared.endpoints[endpoint]) {
                endpointAdded();
            }
            else {
                shared.endpoints.on("add", function(edp) { if (edp.name == endpoint) { endpointAdded(); } });
            }

            resolve();
        });
    };

    this.stop = function() {
        return new Promise(function(resolve, reject) {
            // remove router from endpoint
            if (shared.endpoints[endpoint]) {
                for (k in shared.endpoints[endpoint].routes.get) {
                    if (shared.endpoints[endpoint].routes.get[k].path + "" === route + "") {
                        shared.endpoints[endpoint].routes.get.splice(k,1);
                        break;
                    }
                }
            }

            endpoint = null;
            route = null;

            resolve();
        });
    };
};


/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = TriplexHSP;